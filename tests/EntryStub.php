<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use Linio\Entry;
use Faker\Factory;

Final class EntryStub
{

    public static function create( int $index, string $message) :Entry{
        return new Entry($index,$message);
    }

    public static function random() :Entry{
        $index = Factory::create()->numberBetween(1, 100);
        $message = (string) $index;

        return self::create($index,$message);
    }
}
