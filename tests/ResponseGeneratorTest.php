<?php
declare(strict_types=1);

namespace Test;

use Linio\NumberArray;
use Linio\ResponseGenerator;
use Mockery;
use PHPUnit\Framework\TestCase;

class ResponseGeneratorTest extends TestCase
{
    /** @var NumberArray|Mockery\MockInterface|null */
    private $numberArray;
    /** @var ResponseGenerator|null */
    private $response;

    public function setUp(): void
    {
        parent::setUp();
        $this->numberArray = Mockery::mock(NumberArray::class);
        $this->response = new ResponseGenerator($this->numberArray);
    }

    public function tearDown(): void
    {
        $this->response = null;
    }

    public function test__invoke(): void
    {

        $testResponseContainer = [];
        $container = [];
        for ($i = 1; $i <= 100; $i++) {

            $container[] = $i;
            $index = $i;
            switch ($i) {
                case ($i % 3 === 0 && $i % 5 === 0):
                    $message = 'Linianos';
                    break;
                case ($i % 3 === 0):
                    $message = 'Linio';
                    break;
                case ($i % 5 === 0):
                    $message = 'IT';
                    break;
                default:
                    $message = (string)$i;
            }

            $testResponseContainer[] = EntryStub::create($index, $message)->arrayEntry();
        }

        $this->numberArray->shouldReceive('buildArray')->once()->andReturn($container);

        $testResponse = $this->response->__invoke();

        $this->assertEquals($testResponse, $testResponseContainer);

    }
}
