<?php

namespace Test;

use PHPUnit\Framework\TestCase;
use Linio\MultiChecker;

final class MultiCheckerTest extends TestCase
{

    private $testMultichecker;

    public function setUp(): void
    {
        parent::setUp();
        $this->testMultichecker = new MultiChecker();
    }

    public function tearDown(): void
    {
        $this->testMultichecker = null;
    }

    /**
     * @dataProvider dataProvider
     * @param int $index
     * @param string $message
     */
    public function test__invoke(int $index, string $message) :void
    {
        $entry = $this->testMultichecker->buildCheckedItem($index,$message);
        $this->assertSame($message,$entry->getMessage());
        $this->assertSame($index,$entry->getIndex());
    }

    public function dataProvider(){
        return [
                '1' => [
                    'index' => 1,
                    'message' => '1',
                ],
                '2' => [
                    'index' => 2,
                    'message' => '2',
                ],
                '3' => [
                    'index' => 3,
                    'message' => 'Linio',
                ],
                '4' => [
                    'index' => 4,
                    'message' => '4',
                ],
                '5' => [
                    'index' => 5,
                    'message' => 'IT',
                ],
                '6' => [
                    'index' => 6,
                    'message' => 'Linio',
                ],
                '7' => [
                    'index' => 7,
                    'message' => '7',
                ],
                '8' => [
                    'index' => 8,
                    'message' => '8',
                ],
                '9' => [
                    'index' => 9,
                    'message' => 'Linio',
                ],
                '10' => [
                    'index' => 10,
                    'message' => 'IT',
                ],
                '11' => [
                    'index' => 11,
                    'message' => '11',
                ],
                '12' => [
                    'index' => 12,
                    'message' => 'Linio',
                ],
                '13' => [
                    'index' => 13,
                    'message' => '13',
                ],
                '14' => [
                    'index' => 14,
                    'message' => '14',
                ],
                '15' => [
                    'index' => 100,
                    'message' => 'Linianos',
                ]
           ];
    }
}
