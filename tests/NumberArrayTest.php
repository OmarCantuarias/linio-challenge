<?php

namespace Test;

use Linio\NumberArray;
use PHPUnit\Framework\TestCase;

class NumberArrayTest extends TestCase
{

    private $testNumberArray;

    public function testBuildArray(): void
    {
        $this->testNumberArray = new NumberArray();
        $testList = $this->testNumberArray->buildArray();
        $this->assertCount(100,$testList);
    }
}
