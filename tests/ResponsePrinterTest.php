<?php

namespace Test;

use Linio\ResponsePrinter;
use PHPUnit\Framework\TestCase;

class ResponsePrinterTest extends TestCase
{
    private array $testResponse;
    private $testPrinter;

    public function test__invoke()
    {
        $this->testResponse = [
            '1' => [
                'index' => 1,
                'message' => '1',
            ],
            '2' => [
                'index' => 2,
                'message' => '2',
            ],
        ];

        $this->testPrinter = new ResponsePrinter();
        $this->expectOutputString("1: 1<br />\n2: 2<br />\nCOMPLETE!");
        $this->testPrinter->__invoke($this->testResponse);

    }
}
