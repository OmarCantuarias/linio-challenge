## Challenge – Backend Developer
Write a program that prints all the numbers from 1 to 100. However, for multiples of 3, instead of the number, print "Linio". For multiples of 5 print "IT". For numbers which are multiples of both 3 and 5, print "Linianos".

But here's the catch: you can use only one `if`. No multiple branches, ternary operators or `else`.

### Requirements
* 1 if
* You can't use `else`, `else if` or ternary
* Unit tests
* Feel free to apply your SOLID knowledge
* You can write the challenge in any language you want. Here at Linio we are big fans of PHP, Kotlin and TypeScript

### Installation

    php composer.phar install
    
### Execute
    
The program prints the required numbers automatically, to run the program you cant deploy on a local server or execute the following line

    php index.php
    
### Run Unit Test

Commands must be executed from the project's root:

- On Linux

      php vendor/bin/phpunit tests

- On Windows

      ./vendor/bin/phpunit tests    
    
### Candidate

* Omar Cantuarias Saleh