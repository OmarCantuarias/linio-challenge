<?php

namespace Linio;


use function PHPUnit\Framework\assertObjectHasAttribute;

/**
 * Class ResponsePrinter
 * @package Linio
 *
 * This class takes an array built in ResponseGenerator and prints it in
 * a readable format on html.
 */
class ResponsePrinter
{

    /**
     * @param $responseArray
     */
    public function __invoke($responseArray)
    {

        foreach ($responseArray as $item){
            echo nl2br($item['index'].': '.$item['message']."\n");
        }

        echo 'COMPLETE!';
    }

}