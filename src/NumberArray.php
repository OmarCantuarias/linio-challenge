<?php
declare(strict_types=1);
namespace Linio;

/**
 * Class NumberArray
 * @package Linio
 *
 * This class is charged with building a list from 1 to 100
 */
class NumberArray implements ArrayList
{
    private array $list = [];
    private const MAX_COUNT = 100;

    public function __construct(){
        $this->list= [];
    }

   public function buildArray(): array {

       for ($i=1;$i<=self::MAX_COUNT;$i++) {
            $this->list[] = $i;
       }

       return $this->list;
   }

}