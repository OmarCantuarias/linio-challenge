<?php

namespace Linio;

/**
 * Class ResponseGenerator
 * @package Linio
 *
 * This Class is the one charged with parsing through the NumberArray while asking Multickecker to
 * build a new formatted array, later to be printed by ResponsePrinter.
 */
class ResponseGenerator
{

    /**
     * @var NumberArray
     */
    private NumberArray $numberArray;
    /**
     * @var array
     */
    private array $responseContainer;


    /**
     * ResponseGenerator constructor.
     * @param NumberArray $numberArray
     */
    public function __construct(NumberArray $numberArray)
    {
        $this->numberArray = $numberArray;
        $this->responseContainer = [];
    }

    /**
     * @return array
     */
    public function __invoke()
    {
        $cycles = $this->numberArray->buildArray();
        foreach ($cycles as $cycle){
            $check = new MultiChecker();
            $this->responseContainer[] = $check($cycle)->arrayEntry();
        }

        return $this->responseContainer;
    }
}