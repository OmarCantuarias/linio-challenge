<?php

namespace Linio;

/**
 * Class Entry
 * @package Linio
 *
 *  This class builds an object that contains both an index to check for multiples of 3 or 5
 *  and the final response in message, allowing the class Multichecker to rewrite the message
 *  without losing the original number.
 */
class Entry
{
    /**
     * @var int
     */
    private $index;
    /**
     * @var string
     */
    private $message;

    /**
     * Entry constructor.
     * @param int $index
     * @param string $message
     */
    public function __construct(int $index, string $message)
    {
        $this->index= $index;
        $this->message= $message;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function arrayEntry() : array {
        return [
            'index' => $this->getIndex(),
            'message' => $this->getMessage(),
        ];
    }

}