<?php

namespace Linio;

/**
 * Class MultiChecker
 * @package Linio
 *
 * This class main function is to take a number and return and Entry object already parsed and
 * with the correct message depending if the number is a multiple of 3, 5 or both.
 */
class MultiChecker
{


    const LINIO = 'Linio';

    const IT = 'IT';

    const LILIANOS = 'Linianos';

    /**
     * @var
     */
    private $checkedItem;

    /**
     * @param $entrada
     * @return Entry
     */
    public function __invoke(int $entrada): Entry
    {
        $this->checkedItem = $this->buildCheckedItem($entrada, (string) $entrada);

        $this->multipleCheck($entrada,3,self::LINIO);
        $this->multipleCheck($entrada,5,self::IT);
        $this->multipleCheck($entrada,15,self::LILIANOS);

        return $this->checkedItem;
    }

    /**
     * @param $index
     * @param $message
     * @return Entry
     */
    public function buildCheckedItem($index, $message): Entry
    {
        return new Entry($index,$message);
    }

    /**
     * @param $number
     * @param $multiplier
     * @param $string
     */
    public function multipleCheck($number, $multiplier, $string):void {
        if($number % $multiplier === 0){
            $this->checkedItem = $this->buildCheckedItem($number,$string);
        }
    }
}