<?php


namespace Linio;


interface ArrayList
{
    public function buildArray(): array;
}