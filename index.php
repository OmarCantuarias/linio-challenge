<?php

use Linio\NumberArray;
use Linio\ResponsePrinter;
use Linio\ResponseGenerator;

require_once __DIR__ . '/vendor/autoload.php';

    $numberArray = new NumberArray();
    $responseGenerator = new ResponseGenerator($numberArray);
    $responseArray = $responseGenerator();

    $responsePrinter = new ResponsePrinter();
    $responsePrinter($responseArray);





